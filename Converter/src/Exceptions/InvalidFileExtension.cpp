#include "InvalidFileExtension.h"


Exceptions::InvalidFileExtension::InvalidFileExtension() {
}

Exceptions::InvalidFileExtension::~InvalidFileExtension() throw() {
}

const char *Exceptions::InvalidFileExtension::what() const throw() {
    return "Invalid file extension. Only files with \".txt\" and \".xml\" extensions are acceptable.";
}
