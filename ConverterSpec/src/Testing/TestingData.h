#ifndef CONVERTERXMLTXT_TESTINGDATA_H
#define CONVERTERXMLTXT_TESTINGDATA_H


#include <iostream>


namespace Testing {
    class TestingData {
    public:
        static const char *getCustomTag1();

        static const char *getCustomTag2();

        static const char *getCustomTag3();

        static const char *getFilenameWithInvalidExtension();

        static const char *getInvalidExtension();

        static const char *getReferenceFilenameOnly();

        static const char *getTxtEmptyContent();

        static const char *getTxtExtension();

        static const char *getTxtWithComplexContent();

        static const char *getTxtReferenceContent();

        static const char *getTxtReferenceFilename();

        static const char *getTxtWithAdjacentWhitespaces();

        static const char *getTxtWithNewLine();

        static const char *getXmlContentWithCustomTags();

        static const char *getXmlContentWithMultipleColTags();

        static const char *getXmlContentWithOnlyRootRowAndColTags();

        static const char *getXmlContentWithOnlyRootTag();

        static const char *getXmlExtension();

        static const char *getXmlReferenceContent();

        static const char *getXmlReferenceContentWithCustomTags();

        static const char *getXmlReferenceFilename();

        static const char *getXmlWithComplexContent();

        static const char *getXmlWithInvalidContent();

    private:
        static const char *customTag1;
        static const char *customTag2;
        static const char *customTag3;
        static const char *filenameWithInvalidExtension;
        static const char *invalidExtension;
        static const char *referenceFilenameOnly;
        static const char *txtEmptyContent;
        static const char *txtExtension;
        static const char *txtReferenceContent;
        static const char *txtReferenceFilename;
        static const char *txtWithAdjacentWhitespaces;
        static const char *txtWithComplexContent;
        static const char *txtWithNewLine;
        static const char *xmlContentWithCustomTags;
        static const char *xmlContentWithMultipleColTags;
        static const char *xmlContentWithOnlyRootRowAndColTags;
        static const char *xmlContentWithOnlyRootTag;
        static const char *xmlExtension;
        static const char *xmlReferenceContent;
        static const char *xmlReferenceContentWithCustomTags;
        static const char *xmlReferenceFilename;
        static const char *xmlWithComplexContent;
        static const char *xmlWithInvalidContent;
    };
}


#endif //CONVERTERXMLTXT_TESTINGDATA_H
