#include "FileManager.h"
#include <sstream>


void Utils::FileManager::create(const std::string &file) {
    ensureFileExists(file);
}

std::string Utils::FileManager::read(const std::string &file) {
    std::ifstream f(file.c_str());
    std::stringstream buffer;
    buffer << f.rdbuf();
    return buffer.str();
}

void Utils::FileManager::insert(const std::string &file, const std::string &content) {
    boost::filesystem::ofstream f(file.c_str());
    f << content;
    f.close();
}

bool Utils::FileManager::exist(const std::string &file) {
    boost::filesystem::path filepath = file;
    return boost::filesystem::exists(filepath);
}

void Utils::FileManager::ensureFileExists(const std::string &file) {
    boost::filesystem::path filepath = file;
    boost::filesystem::ofstream f(filepath);
    f.close();
}
