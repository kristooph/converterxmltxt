#include "XMLParserError.h"


Exceptions::XMLParserError::XMLParserError() {
}

Exceptions::XMLParserError::~XMLParserError() throw() {
}

const char *Exceptions::XMLParserError::what() const throw() {
    return "XML Parser Exception.";
}
