#include <gtest/gtest.h>
#include <src/Exceptions/XMLParserError.h>
#include "CantOpenFile.h"
#include "ContentProcessor.h"
#include "ContentProcessorFixture.h"
#include "Converter.h"
#include "ConverterFixture.h"
#include "FileManager.h"
#include "FileManagerFixture.h"
#include "FilenameProcessor.h"
#include "FilenameProcessorFixture.h"
#include "InvalidFileExtension.h"
#include "TestingData.h"
#include "TestingFileManager.h"
#include "TestingTools.h"


TEST_F(ContentProcessorFixture, createsTxtEmptyContentWhenProcessingXmlContentWithOnlyRootRowAndColTags) {
    const char *xmlExtension = Testing::TestingData::getXmlExtension();
    const char *xmlContent = Testing::TestingData::getXmlContentWithOnlyRootRowAndColTags();
    const char *expectedContent = Testing::TestingData::getTxtEmptyContent();
    std::string actualContent = Utils::ContentProcessor::createTargetContent(xmlContent, xmlExtension);
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(ContentProcessorFixture, createsTxtEmptyContentWhenProcessingXmlContentWithOnlyRootTag) {
    const char *xmlExtension = Testing::TestingData::getXmlExtension();
    const char *xmlContent = Testing::TestingData::getXmlContentWithOnlyRootTag();
    const char *expectedContent = Testing::TestingData::getTxtEmptyContent();
    std::string actualContent = Utils::ContentProcessor::createTargetContent(xmlContent, xmlExtension);
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(ContentProcessorFixture, createsTxtReferenceContentWhenProcessingXmlReferenceContent) {
    const char *xmlExtension = Testing::TestingData::getXmlExtension();
    const char *xmlReferenceContent = Testing::TestingData::getXmlReferenceContent();
    const char *expectedContent = Testing::TestingData::getTxtReferenceContent();
    std::string actualContent = Utils::ContentProcessor::createTargetContent(xmlReferenceContent, xmlExtension);
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(ContentProcessorFixture, createsTxtReferenceContentWhenProcessingXmlReferenceContentWithCustomTags) {
    const char *xmlExtension = Testing::TestingData::getXmlExtension();
    const char *xmlContent = Testing::TestingData::getXmlReferenceContentWithCustomTags();
    const char *tag1 = Testing::TestingData::getCustomTag1();
    const char *tag2 = Testing::TestingData::getCustomTag2();
    const char *tag3 = Testing::TestingData::getCustomTag3();
    std::string actualContent = Utils::ContentProcessor::createTargetContent(xmlContent, xmlExtension, tag1, tag2, tag3);
    std::string expectedContent = Testing::TestingData::getTxtReferenceContent();
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(ContentProcessorFixture, createsTxtWithExpectedContentWhenProcessingXmlWithComplexContent) {
    const char *xmlExtension = Testing::TestingData::getXmlExtension();
    const char *xmlContent = Testing::TestingData::getXmlWithComplexContent();
    const char *expectedContent = Testing::TestingData::getTxtWithComplexContent();
    std::string actualContent = Utils::ContentProcessor::createTargetContent(xmlContent, xmlExtension);
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(ContentProcessorFixture, createsXmlContentWithCustomTagsWhenProcessingTxtWithNewLineWhenCustomTagsAreProvided) {
    const char *txtExtension = Testing::TestingData::getTxtExtension();
    const char *txtContent = Testing::TestingData::getTxtWithNewLine();
    const char *tag1 = Testing::TestingData::getCustomTag1();
    const char *tag2 = Testing::TestingData::getCustomTag2();
    const char *tag3 = Testing::TestingData::getCustomTag3();
    const char *expectedContent = Testing::TestingData::getXmlContentWithCustomTags();
    std::string actualContent = Utils::ContentProcessor::createTargetContent(txtContent, txtExtension, tag1, tag2, tag3);
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(ContentProcessorFixture, createsXmlContentWithMultipleColTagsWhenProcessingTxtWithAdjacentWhitespaces) {
    const char *txtExtension = Testing::TestingData::getTxtExtension();
    const char *txtContent = Testing::TestingData::getTxtWithAdjacentWhitespaces();
    const char *expectedContent = Testing::TestingData::getXmlContentWithMultipleColTags();
    std::string actualContent = Utils::ContentProcessor::createTargetContent(txtContent, txtExtension);
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(ContentProcessorFixture, createsXmlContentWithOnlyRootRowAndColTagsWhenProcessingTxtWithNewLine) {
    const char *txtExtension = Testing::TestingData::getTxtExtension();
    const char *txtContent = Testing::TestingData::getTxtWithNewLine();
    const char *expectedContent = Testing::TestingData::getXmlContentWithOnlyRootRowAndColTags();
    std::string actualContent = Utils::ContentProcessor::createTargetContent(txtContent, txtExtension);
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(ContentProcessorFixture, createsXmlContentWithOnlyRootTagWhenProcessingTxtEmptyContent) {
    const char *txtExtension = Testing::TestingData::getTxtExtension();
    const char *txtContent = Testing::TestingData::getTxtEmptyContent();
    const char *expectedContent = Testing::TestingData::getXmlContentWithOnlyRootTag();
    std::string actualContent = Utils::ContentProcessor::createTargetContent(txtContent, txtExtension);
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(ContentProcessorFixture, createsXmlReferenceContentWhenProcessingTxtReferenceContent) {
    const char *txtExtension = Testing::TestingData::getTxtExtension();
    const char *txtReferenceContent = Testing::TestingData::getTxtReferenceContent();
    const char *expectedContent = Testing::TestingData::getXmlReferenceContent();
    std::string actualContent = Utils::ContentProcessor::createTargetContent(txtReferenceContent, txtExtension);
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(ContentProcessorFixture, throwsWhenCreatingTargetContentForFilenameWithInvalidExtension) {
    const char *invalidExtension = Testing::TestingData::getInvalidExtension();
    const char *content = Testing::TestingData::getTxtEmptyContent();
    EXPECT_THROW(Utils::ContentProcessor::createTargetContent(content, invalidExtension), Exceptions::InvalidFileExtension);
}

TEST_F(ContentProcessorFixture, throwsWhenProcessingXmlWithInvalidContent) {
    const char *xmlExtension = Testing::TestingData::getXmlExtension();
    const char *invalidXmlContent = Testing::TestingData::getXmlWithInvalidContent();
    EXPECT_THROW(Utils::ContentProcessor::createTargetContent(invalidXmlContent, xmlExtension), Exceptions::XMLParserError);
}

TEST_F(ConverterFixture, throwsWhenProcessingNotExistingFile) {
    const char *notExistingFile = Testing::TestingData::getTxtReferenceFilename();
    Testing::TestingTools::ensureFileDoesNotExist(notExistingFile);
    EXPECT_THROW(Utils::Converter::process(notExistingFile), Exceptions::CantOpenFile);
}

TEST_F(FileManagerFixture, checksIfFileExists) {
    const char *filename = Testing::TestingData::getTxtReferenceFilename();
    Testing::TestingFileManager::create(filename);
    EXPECT_TRUE(Utils::FileManager::exist(filename));
}

TEST_F(FileManagerFixture, createsFile) {
    const char *filename = Testing::TestingData::getTxtReferenceFilename();
    Utils::FileManager::create(filename);
    EXPECT_TRUE(Testing::TestingFileManager::exist(filename));
}

TEST_F(FileManagerFixture, insertsContentToFile) {
    const char *filename = Testing::TestingData::getTxtReferenceFilename();
    const char *expectedContent = Testing::TestingData::getTxtReferenceContent();
    Testing::TestingFileManager::create(filename);
    Utils::FileManager::insert(filename, expectedContent);
    std::string actualContent = Testing::TestingFileManager::read(filename);
    EXPECT_EQ(actualContent, expectedContent);
}

TEST_F(FileManagerFixture, returnsContentWhenReadingFile) {
    const char *filename = Testing::TestingData::getTxtReferenceFilename();
    const char *content = Testing::TestingData::getTxtReferenceContent();
    Testing::TestingTools::createReferenceTxtFile();
    std::string sut = Utils::FileManager::read(filename);
    EXPECT_EQ(sut, content);
}

TEST_F(FilenameProcessorFixture, createsExpectedTxtFilenameWhenCreatingTargetFilenameForReferenceXmlFilename) {
    std::string xmlExtension = Testing::TestingData::getXmlExtension();
    std::string xmlReferenceFilename = Testing::TestingData::getXmlReferenceFilename();
    std::string expectedFilename = Testing::TestingData::getTxtReferenceFilename();
    std::string actualFilename = Utils::FilenameProcessor::createTargetFilename(xmlReferenceFilename, xmlExtension);
    EXPECT_EQ(actualFilename, expectedFilename);
}

TEST_F(FilenameProcessorFixture, createsExpectedXmlFilenameWhenCreatingTargetFilenameForReferenceTxtFilename) {
    std::string txtExtension = Testing::TestingData::getTxtExtension();
    std::string txtReferenceFilename = Testing::TestingData::getTxtReferenceFilename();
    std::string expectedFilename = Testing::TestingData::getXmlReferenceFilename();
    std::string actualFilename = Utils::FilenameProcessor::createTargetFilename(txtReferenceFilename, txtExtension);
    EXPECT_EQ(actualFilename, expectedFilename);
}

TEST_F(FilenameProcessorFixture, extractsExpectedExtensionWhenExtractingExtensionFromFilename) {
    std::string filename = Testing::TestingData::getTxtReferenceFilename();
    const char *expectedExtension = Testing::TestingData::getTxtExtension();
    std::string actualExtension = Utils::FilenameProcessor::extractExtension(filename);
    EXPECT_EQ(actualExtension, expectedExtension);
}

TEST_F(FilenameProcessorFixture, extractsExpectedNameWhenExtractingNameFromFilename) {
    std::string filename = Testing::TestingData::getTxtReferenceFilename();
    const char *expectedName = Testing::TestingData::getReferenceFilenameOnly();
    std::string actualName = Utils::FilenameProcessor::extractName(filename);
    EXPECT_EQ(actualName, expectedName);
}

TEST_F(FilenameProcessorFixture, throwsWhenCreatingFilenameForFilenameWithInvalidExtension) {
    const char *filenameWithInvalidExtension = Testing::TestingData::getFilenameWithInvalidExtension();
    const char *invalidExtension = Testing::TestingData::getInvalidExtension();
    EXPECT_THROW(Utils::FilenameProcessor::createTargetFilename(filenameWithInvalidExtension, invalidExtension), Exceptions::InvalidFileExtension);
}

TEST_F(FilenameProcessorFixture, throwsWhenExtractingExtensionFromFilenameWithInvalidExtension) {
    const char *filenameWithInvalidExtension = Testing::TestingData::getFilenameWithInvalidExtension();
    EXPECT_THROW(Utils::FilenameProcessor::extractExtension(filenameWithInvalidExtension), Exceptions::InvalidFileExtension);
}