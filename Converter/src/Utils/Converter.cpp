#include "Converter.h"
#include "CantOpenFile.h"
#include "ContentProcessor.h"
#include "FileManager.h"
#include "FilenameProcessor.h"
#include "InvalidFileExtension.h"


void Utils::Converter::process(const std::string &file,
                               const std::string &tag1,
                               const std::string &tag2,
                               const std::string &tag3) {
    if (!FileManager::exist(file)) {
        throw Exceptions::CantOpenFile();
    }
    const std::string &extension = FilenameProcessor::extractExtension(file);
    const std::string &content = FileManager::read(file);
    const std::string &targetContent = ContentProcessor::createTargetContent(content, extension, tag1, tag2, tag3);
    const std::string &targetFilename = FilenameProcessor::createTargetFilename(file, extension);
    FileManager::create(targetFilename);
    FileManager::insert(targetFilename, targetContent);
}
