#ifndef CONVERTERXMLTXT_FILENAMEPROCESSOR_H
#define CONVERTERXMLTXT_FILENAMEPROCESSOR_H


#include <iostream>


namespace Utils {
    class FilenameProcessor {
    public:
        static std::string extractExtension(const std::string &file);

        static std::string extractName(const std::string &file);

        static std::string createTargetFilename(const std::string &file, const std::string &extension);

    private:
        static std::string createForXml(const std::string &file);

        static std::string createForTxt(const std::string &file);
    };
}


#endif //CONVERTERXMLTXT_FILENAMEPROCESSOR_H
