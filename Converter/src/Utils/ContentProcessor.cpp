#include "ContentProcessor.h"
#include <boost/algorithm/string.hpp>
#include <tinyxml2.h>
#include "FileManager.h"
#include "FilenameProcessor.h"
#include "InvalidFileExtension.h"
#include "XMLParserError.h"


std::string Utils::ContentProcessor::createTargetContent(const std::string &content,
                                                         const std::string &extension,
                                                         const std::string &tag1,
                                                         const std::string &tag2,
                                                         const std::string &tag3) {
    if (extension == ".txt") {
        return createForXml(content, tag1, tag2, tag3);
    } else if (extension == ".xml") {
        return createForTxt(content, tag1, tag2, tag3);
    } else {
        throw Exceptions::InvalidFileExtension();
    }
}

std::string Utils::ContentProcessor::createForXml(const std::string &content,
                                                  const std::string &tag1,
                                                  const std::string &tag2,
                                                  const std::string &tag3) {
    tinyxml2::XMLDocument document;
    tinyxml2::XMLElement *element1 = document.NewElement(tag1.c_str());
    document.LinkEndChild(element1);
    std::istringstream input(content);
    std::string line;
    while (getline(input, line)) {
        tinyxml2::XMLElement *element2 = document.NewElement(tag2.c_str());
        std::vector<std::string> words;
        boost::split(words, line, boost::is_any_of(" "));
        for (std::vector<std::string>::iterator it = words.begin(); it != words.end(); ++it) {
            tinyxml2::XMLElement *element3 = document.NewElement(tag3.c_str());
            element3->LinkEndChild(document.NewText(it->c_str()));
            element2->LinkEndChild(element3);
        }
        element1->LinkEndChild(element2);
    }

    tinyxml2::XMLPrinter printer;
    document.Print(&printer);
    return printer.CStr();
}

std::string Utils::ContentProcessor::createForTxt(const std::string &content,
                                                  const std::string &tag1,
                                                  const std::string &tag2,
                                                  const std::string &tag3) {
    tinyxml2::XMLDocument document;
    document.Parse(content.c_str());
    if (document.ErrorID()) {
        throw Exceptions::XMLParserError();
    }

    const char *tag2name = tag2.c_str();
    const char *tag3name = tag3.c_str();
    tinyxml2::XMLElement *element1;
    tinyxml2::XMLElement *element2;
    tinyxml2::XMLElement *lastElement2;
    tinyxml2::XMLElement *element3;
    tinyxml2::XMLElement *lastElement3;
    tinyxml2::XMLText *textNode;
    std::string targetContent;

    element1 = document.FirstChildElement(tag1.c_str());
    if (element1->NoChildren()) {
        return targetContent;
    }
    element2 = element1->FirstChildElement(tag2name);
    if (element2->NoChildren()) {
        return targetContent;
    }
    lastElement2 = element1->LastChildElement(tag2name);

    while (true) {
        element3 = element2->FirstChildElement(tag3name);
        lastElement3 = element2->LastChildElement(tag3name);
        while (true) {
            if (!element3->NoChildren()) {
                textNode = element3->FirstChild()->ToText();
                if (textNode != NULL) {
                    targetContent += textNode->Value();
                }
            }
            if (element3 == lastElement3) {
                break;
            }
            targetContent += " ";
            element3 = element3->NextSiblingElement(tag3name);
        }
        if (element2 == lastElement2) {
            break;
        }
        targetContent += "\n";
        element2 = element2->NextSiblingElement(tag2name);
    }

    return targetContent;
}
