#ifndef CONVERTERXMLTXT_CANTOPENFILE_H
#define CONVERTERXMLTXT_CANTOPENFILE_H


#include <exception>


namespace Exceptions {
    class CantOpenFile : public std::exception {
    public:
        CantOpenFile();

        ~CantOpenFile() throw();

        const char *what() const throw();
    };
}


#endif //CONVERTERXMLTXT_CANTOPENFILE_H
