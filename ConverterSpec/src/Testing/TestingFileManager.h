#ifndef CONVERTERXMLTXT_TESTINGFILEMANAGER_H
#define CONVERTERXMLTXT_TESTINGFILEMANAGER_H


#include <boost/filesystem.hpp>
#include <iostream>


namespace Testing {
    class TestingFileManager {
    public:
        static void create(const std::string &file);

        static std::string read(const std::string &file);

        static void remove(const std::string &file);

        static void insert(const std::string &file, const std::string &content);

        static bool exist(const std::string &file);

    private:
        static void ensureFileExists(const boost::filesystem::path &filepath);
    };
}


#endif //CONVERTERXMLTXT_TESTINGFILEMANAGER_H
