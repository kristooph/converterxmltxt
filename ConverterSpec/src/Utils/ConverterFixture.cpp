#include "ConverterFixture.h"
#include "TestingData.h"
#include "TestingTools.h"


void ConverterFixture::TearDown() {
    Test::TearDown();
    Testing::TestingTools::ensureFileDoesNotExist(Testing::TestingData::getTxtReferenceFilename());
    Testing::TestingTools::ensureFileDoesNotExist(Testing::TestingData::getXmlReferenceFilename());
    Testing::TestingTools::ensureFileDoesNotExist(Testing::TestingData::getFilenameWithInvalidExtension());
}
