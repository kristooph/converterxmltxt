#include "TestingTools.h"
#include "TestingData.h"
#include "TestingFileManager.h"


void Testing::TestingTools::createReferenceTxtFile() {
    TestingFileManager::create(TestingData::getTxtReferenceFilename());
    TestingFileManager::insert(TestingData::getTxtReferenceFilename(), TestingData::getTxtReferenceContent());
}

void Testing::TestingTools::createXmlReferenceFile() {
    TestingFileManager::create(TestingData::getXmlReferenceFilename());
    TestingFileManager::insert(TestingData::getXmlReferenceFilename(), TestingData::getXmlReferenceContent());
}

void Testing::TestingTools::createFileWithInvalidExtension() {
    TestingFileManager::create(TestingData::getFilenameWithInvalidExtension());
}

void Testing::TestingTools::ensureFileDoesNotExist(const std::string &file) {
    if (TestingFileManager::exist(file)) {
        TestingFileManager::remove(file);
    }
}
