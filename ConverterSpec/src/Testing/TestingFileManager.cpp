#include "TestingFileManager.h"
#include <sstream>


void Testing::TestingFileManager::create(const std::string &file) {
    boost::filesystem::path filepath = file;
    ensureFileExists(filepath);
}

std::string Testing::TestingFileManager::read(const std::string &file) {
    std::ifstream f(file.c_str());
    std::stringstream buffer;
    buffer << f.rdbuf();
    return buffer.str();
}

void Testing::TestingFileManager::remove(const std::string &file) {
    boost::filesystem::path filepath = file;
    boost::filesystem::remove(filepath);
}

void Testing::TestingFileManager::insert(const std::string &file, const std::string &content) {
    boost::filesystem::ofstream f(file.c_str());
    f << content;
    f.close();
}

bool Testing::TestingFileManager::exist(const std::string &file) {
    boost::filesystem::path filepath = file;
    return boost::filesystem::exists(filepath);
}

void Testing::TestingFileManager::ensureFileExists(const boost::filesystem::path &filepath) {
    boost::filesystem::ofstream file(filepath);
    file.close();
}
