#ifndef CONVERTERXMLTXT_CONTENTPROCESSOR_H
#define CONVERTERXMLTXT_CONTENTPROCESSOR_H


#include <iostream>


namespace Utils {
    class ContentProcessor {
    public:
        static std::string createTargetContent(const std::string &content,
                                               const std::string &extension,
                                               const std::string &tag1 = "root",
                                               const std::string &tag2 = "row",
                                               const std::string &tag3 = "col");

    private:
        static std::string createForXml(const std::string &content,
                                        const std::string &tag1,
                                        const std::string &tag2,
                                        const std::string &tag3);

        static std::string createForTxt(const std::string &content,
                                        const std::string &tag1,
                                        const std::string &tag2,
                                        const std::string &tag3);
    };
}


#endif //CONVERTERXMLTXT_CONTENTPROCESSOR_H
