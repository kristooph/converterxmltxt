#include "CantOpenFile.h"


Exceptions::CantOpenFile::CantOpenFile() {
}

Exceptions::CantOpenFile::~CantOpenFile() throw() {
}

const char *Exceptions::CantOpenFile::what() const throw() {
    return "Can't open file.";
}
