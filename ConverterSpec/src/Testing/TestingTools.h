#ifndef CONVERTERXMLTXT_TESTINGTOOLS_H
#define CONVERTERXMLTXT_TESTINGTOOLS_H


#include <iostream>


namespace Testing {
    class TestingTools {
    public:
        static void createReferenceTxtFile();

        static void createXmlReferenceFile();

        static void createFileWithInvalidExtension();

        static void ensureFileDoesNotExist(const std::string &file);
    };
}


#endif //CONVERTERXMLTXT_TESTINGTOOLS_H
