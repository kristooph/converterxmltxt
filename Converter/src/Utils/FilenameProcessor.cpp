#include "FilenameProcessor.h"
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/filesystem.hpp>
#include "InvalidFileExtension.h"


std::string Utils::FilenameProcessor::extractExtension(const std::string &file) {
    std::string extension = boost::filesystem::extension(file);
    boost::algorithm::to_lower(extension);
    if (extension != ".txt" && extension != ".xml") {
        throw Exceptions::InvalidFileExtension();
    }
    return extension;

}

std::string Utils::FilenameProcessor::extractName(const std::string &file) {
    boost::filesystem::path filepath = file;
    return filepath.stem().string();
}

std::string Utils::FilenameProcessor::createTargetFilename(const std::string &file, const std::string &extension) {
    if (extension == ".txt") {
        return createForXml(file);
    } else if (extension == ".xml") {
        return createForTxt(file);
    } else {
        throw Exceptions::InvalidFileExtension();
    }
}

std::string Utils::FilenameProcessor::createForXml(const std::string &file) {
    std::string name = extractName(file);
    return name += ".xml";
}


std::string Utils::FilenameProcessor::createForTxt(const std::string &file) {
    std::string name = extractName(file);
    return name += ".txt";
}
