#ifndef CONVERTERXMLTXT_CONVERTER_H
#define CONVERTERXMLTXT_CONVERTER_H


#include <iostream>


namespace Utils {
    class Converter {
    public:
        static void process(const std::string &file,
                            const std::string &tag1 = "root",
                            const std::string &tag2 = "row",
                            const std::string &tag3 = "col");
    };
}


#endif //CONVERTERXMLTXT_CONVERTER_H
