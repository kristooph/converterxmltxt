#ifndef CONVERTERXMLTXT_INVALIDFILEEXTENSION_H
#define CONVERTERXMLTXT_INVALIDFILEEXTENSION_H


#include <exception>


namespace Exceptions {
    class InvalidFileExtension : public std::exception {
    public:
        InvalidFileExtension();

        ~InvalidFileExtension() throw();

        const char *what() const throw();
    };
}


#endif //CONVERTERXMLTXT_INVALIDFILEEXTENSION_H
