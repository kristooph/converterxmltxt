#include "FileManagerFixture.h"
#include "TestingData.h"
#include "TestingTools.h"


void FileManagerFixture::TearDown() {
    Test::TearDown();
    Testing::TestingTools::ensureFileDoesNotExist(Testing::TestingData::getTxtReferenceFilename());
    Testing::TestingTools::ensureFileDoesNotExist(Testing::TestingData::getXmlReferenceFilename());
    Testing::TestingTools::ensureFileDoesNotExist(Testing::TestingData::getFilenameWithInvalidExtension());
}
