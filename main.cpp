#include "Converter.h"

#define PROGRAM_NAME 0
#define FILENAME 1
#define TAG1 2
#define TAG2 3
#define TAG3 4


int main(int argc, char *argv[]) {

    switch (argc) {
        case 2:
            Utils::Converter::process(argv[FILENAME]);
            return 0;
        case 3:
            Utils::Converter::process(argv[FILENAME], argv[TAG1]);
            return 0;
        case 4:
            Utils::Converter::process(argv[FILENAME], argv[TAG1], argv[TAG2]);
            return 0;
        case 5:
            Utils::Converter::process(argv[FILENAME], argv[TAG1], argv[TAG2], argv[TAG3]);
            return 0;
        default:
            std::cerr << "Usage: " << argv[PROGRAM_NAME] << " <file.ext> [tag1 [tag2 [tag3]]]" << std::endl;
            return 1;
    }
}
