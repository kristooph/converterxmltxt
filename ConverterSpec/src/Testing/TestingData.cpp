#include "TestingData.h"


const char *Testing::TestingData::getCustomTag1() {
        return customTag1;
}

const char *Testing::TestingData::getCustomTag2() {
        return customTag2;
}

const char *Testing::TestingData::getCustomTag3() {
        return customTag3;
}

const char *Testing::TestingData::getFilenameWithInvalidExtension() {
        return filenameWithInvalidExtension;
}

const char *Testing::TestingData::getInvalidExtension() {
        return invalidExtension;
}

const char *Testing::TestingData::getReferenceFilenameOnly() {
        return referenceFilenameOnly;
}

const char *Testing::TestingData::getTxtEmptyContent() {
        return txtEmptyContent;
}

const char *Testing::TestingData::getTxtExtension() {
        return txtExtension;
}

const char *Testing::TestingData::getTxtReferenceContent() {
        return txtReferenceContent;
}

const char *Testing::TestingData::getTxtReferenceFilename() {
        return txtReferenceFilename;
}

const char *Testing::TestingData::getTxtWithAdjacentWhitespaces() {
        return txtWithAdjacentWhitespaces;
}

const char *Testing::TestingData::getTxtWithComplexContent() {
        return txtWithComplexContent;
}

const char *Testing::TestingData::getTxtWithNewLine() {
        return txtWithNewLine;
}

const char *Testing::TestingData::getXmlContentWithCustomTags() {
        return xmlContentWithCustomTags;
};

const char *Testing::TestingData::getXmlContentWithMultipleColTags() {
        return xmlContentWithMultipleColTags;
}

const char *Testing::TestingData::getXmlContentWithOnlyRootRowAndColTags() {
        return xmlContentWithOnlyRootRowAndColTags;
}

const char *Testing::TestingData::getXmlContentWithOnlyRootTag() {
        return xmlContentWithOnlyRootTag;
}

const char *Testing::TestingData::getXmlExtension() {
        return xmlExtension;
}

const char *Testing::TestingData::getXmlReferenceContent() {
        return xmlReferenceContent;
}

const char *Testing::TestingData::getXmlReferenceContentWithCustomTags() {
        return xmlReferenceContentWithCustomTags;
}

const char *Testing::TestingData::getXmlReferenceFilename() {
        return xmlReferenceFilename;
}

const char *Testing::TestingData::getXmlWithComplexContent() {
        return xmlWithComplexContent;
};

const char *Testing::TestingData::getXmlWithInvalidContent() {
        return xmlWithInvalidContent;
};

const char *Testing::TestingData::customTag1 =
        "foo";

const char *Testing::TestingData::customTag2 =
        "bar";

const char *Testing::TestingData::customTag3 =
        "baz";

const char *Testing::TestingData::filenameWithInvalidExtension =
        "invalid.ini";

const char *Testing::TestingData::invalidExtension =
        ".ini";

const char *Testing::TestingData::referenceFilenameOnly =
        "referenceFile";

const char *Testing::TestingData::txtEmptyContent =
        "";

const char *Testing::TestingData::txtExtension =
        ".txt";

const char *Testing::TestingData::txtReferenceContent =
        "1 2 3\n"
        "ala ma kota";

const char *Testing::TestingData::txtReferenceFilename =
        "referenceFile.txt";

const char *Testing::TestingData::txtWithAdjacentWhitespaces =
        "  double  spaces  ";

const char *Testing::TestingData::txtWithComplexContent =
        "THIS  AND THIS AFTER 2 SPACES\n"
        "AND THIS IN NEW LINE  AND THIS AFTER 2 SPACES\n"
        "AND THIS";

const char *Testing::TestingData::txtWithNewLine =
        "\n";

const char *Testing::TestingData::xmlContentWithCustomTags =
        "<foo>\n"
        "    <bar>\n"
        "        <baz></baz>\n"
        "    </bar>\n"
        "</foo>\n";

const char *Testing::TestingData::xmlContentWithMultipleColTags =
        "<root>\n"
        "    <row>\n"
        "        <col></col>\n"
        "        <col></col>\n"
        "        <col>double</col>\n"
        "        <col></col>\n"
        "        <col>spaces</col>\n"
        "        <col></col>\n"
        "        <col></col>\n"
        "    </row>\n"
        "</root>\n";

const char *Testing::TestingData::xmlContentWithOnlyRootRowAndColTags =
        "<root>\n"
        "    <row>\n"
        "        <col></col>\n"
        "    </row>\n"
        "</root>\n";

const char *Testing::TestingData::xmlContentWithOnlyRootTag =
        "<root/>\n";

const char *Testing::TestingData::xmlExtension =
        ".xml";

const char *Testing::TestingData::xmlReferenceContent =
        "<root>\n"
        "    <row>\n"
        "        <col>1</col>\n"
        "        <col>2</col>\n"
        "        <col>3</col>\n"
        "    </row>\n"
        "    <row>\n"
        "        <col>ala</col>\n"
        "        <col>ma</col>\n"
        "        <col>kota</col>\n"
        "    </row>\n"
        "</root>\n";

const char *Testing::TestingData::xmlReferenceContentWithCustomTags =
        "<foo>\n"
        "    <bar>\n"
        "        <baz>1</baz>\n"
        "        <baz>2</baz>\n"
        "        <baz>3</baz>\n"
        "    </bar>\n"
        "    <bar>\n"
        "        <baz>ala</baz>\n"
        "        <baz>ma</baz>\n"
        "        <baz>kota</baz>\n"
        "    </bar>\n"
        "</foo>\n";

const char *Testing::TestingData::xmlReferenceFilename =
        "referenceFile.xml";

const char *Testing::TestingData::xmlWithComplexContent =
        "<root>\n"
        "    not this\n"
        "    <foo>\n"
        "        not this\n"
        "        <bar>not this</bar>\n"
        "        <col>not this</col>\n"
        "    </foo>\n"
        "    <row>\n"
        "        not this\n"
        "        <baz>not this</baz>\n"
        "        <col>THIS</col>\n"
        "        <col></col>\n"
        "        <col>AND THIS AFTER 2 SPACES</col>\n"
        "        <baz>not this</baz>\n"
        "    </row>\n"
        "    <foo>\n"
        "        <bar>not this</bar>\n"
        "    </foo>\n"
        "    <row>\n"
        "        <baz>not this</baz>\n"
        "        <col>AND THIS IN NEW LINE</col>\n"
        "        <col>\n"
        "            <qux>not this</qux>\n"
        "        </col>\n"
        "        <col>AND THIS AFTER 2 SPACES</col>\n"
        "        <baz>not this</baz>\n"
        "    </row>\n"
        "    <row>\n"
        "        <col>AND THIS<qux>not this</qux>however, text after other tag is discarded</col>\n"
        "    </row>\n"
        "    <foo>\n"
        "        <bar>not this</bar>\n"
        "    </foo>\n"
        "</root>\n";

const char *Testing::TestingData::xmlWithInvalidContent =
        "<root>\n"
        "    <row>\n"
        "    </row>\n";
