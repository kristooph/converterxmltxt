#ifndef CONVERTERXMLTXT_XMLPARSERERROR_H
#define CONVERTERXMLTXT_XMLPARSERERROR_H


#include <exception>


namespace Exceptions {
    class XMLParserError : public std::exception {
    public:
        XMLParserError();

        ~XMLParserError() throw();

        const char *what() const throw();
    };
}


#endif //CONVERTERXMLTXT_XMLPARSERERROR_H
