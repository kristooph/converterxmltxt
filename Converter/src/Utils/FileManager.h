#ifndef CONVERTERXMLTXT_FILEMANAGER_H
#define CONVERTERXMLTXT_FILEMANAGER_H


#include <boost/filesystem.hpp>
#include <iostream>


namespace Utils {
    class FileManager {
    public:
        static void create(const std::string &file);

        static std::string read(const std::string &file);

        static void insert(const std::string &file, const std::string &content);

        static bool exist(const std::string &file);

    private:
        static void ensureFileExists(const std::string &file);
    };
}


#endif //CONVERTERXMLTXT_FILEMANAGER_H
