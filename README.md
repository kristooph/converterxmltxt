Converter .xml / .txt
=====================

Description
-----------

This project allows to convert files:

* .xml to .txt;
* .txt to .xml.

Target file will be created based on source file's content. Program will update analogous file or create a new one if it doesn't exist. Target will have the same name as given in argument line, but opposite extension (.xml for .txt files and .txt for .xml).

Program expects one mandatory command line argument -- a file to convert. Optionally you can add up to three tags to look for in .xml / create .xml with these tags. Default tags are `root`, `row` and `col`. Syntax for running is:  
`<executable_name> <file.ext> [tag1 [tag2 [tag3]]]`.

Program will process data that are *only* on specific nesting level in .xml. Second tag (default `row`) in .xml is separated by newline character in .txt. Third tag (default `col`) is separated by whitespace in .txt. See following examples.

Examples
--------

### Default use case

Assuming that `data.xml`'s content is:
~~~~
<root>
    Text in this line won't be processed.
    <row>
        Text in this line won't be processed either.
        <col>This text can be processed and will be added to current line.</col>
        <col>
            <beyond>This text won't be processed, but will add "" to current line.</beyond>
        </col>
        <col>"col" tags are separated by whitespace.</col>
        "row" tags are separated by newline character.
    </row>
    <row>
        <col>This text will be added to the second line.</col>
    </row>
</root>

~~~~

and it exist in directory with executable, opening command prompt there and running:  
`ConverterXmlTxt.exe data.xml`  
will create (or update) `data.txt` file. It will have following content:
~~~~
This text can be processed and will be added to current line.  "col" tags are separated by whitespace.
This text will be added to the second line.
~~~~

Processing such `data.txt` with  
`ConverterXmlTxt.exe data.txt`  
  would give `data.xml` following content:
~~~~
<root>
    <row>
        <col>This</col>
        <col>text</col>
        <col>can</col>
        <col>be</col>
        <col>processed</col>
        <col>and</col>
        <col>will</col>
        <col>be</col>
        <col>added</col>
        <col>to</col>
        <col>current</col>
        <col>line.</col>
        <col></col>
        <col>"col"</col>
        <col>tags</col>
        <col>are</col>
        <col>separated</col>
        <col>by</col>
        <col>whitespace.</col>
    </row>
    <row>
        <col>This</col>
        <col>text</col>
        <col>will</col>
        <col>be</col>
        <col>added</col>
        <col>to</col>
        <col>the</col>
        <col>second</col>
        <col>line.</col>
    </row>
</root>

~~~~

### Using tags

You can specify tags as command line arguments to find them when parsing .xml or create .xml file with them when processing .txt file. This can be useful when your .xml doesn't follow `root` `row` `col` tags convention. When declaring tags order is important; default values are assumed for any undefined tags. For example, when running  
`ConverterXmlTxt.exe data.xml myTag`  
only `tag1` will be updated -- program will assume that `tag1 = myTag`, `tag2 = row`, `tag3 = col`.

Assuming your `data2.xml` file has following content:
~~~~
<customTag1>
    <customTag2>
        <customTag3>1</customTag3>
        <customTag3>2</customTag3>
    </customTag2>
    <customTag2>
        <customTag3>foo</customTag3>
        <customTag3>bar</customTag3>
    </customTag2>
</customTag1>

~~~~

the only way to process it correctly is to run executable with following command:  
`ConverterXmlTxt.exe data2.xml customTag1 customTag2 customTag3`.  
It would create following `data2.txt` file:
~~~~
1 2
foo bar
~~~~

If you want to give your own tags when converting from this .txt to .xml, use:  
`ConverterXmlTxt.exe data2.txt newTag1 newTag2`.  
This produces `data2.xml` with following content:
~~~~
<newTag1>
    <newTag2>
        <col>1</col>
        <col>2</col>
    </newTag2>
    <newTag2>
        <col>foo</col>
        <col>bar</col>
    </newTag2>
</newTag1>

~~~~
Note that unspecified third tag used its default value `col`.

Dependencies
------------

Project uses:

* [Boost libraries 1.68.0](https://www.boost.org/);
* Filesystem Boost Library Binary;
* Regex Boost Library Binary;
* [TinyXML-2](http://www.grinninglizard.com/tinyxml2/);
* [Google Test](https://github.com/google/googletest).

UML diagrams were created with [PlantUML](http://plantuml.com/).
